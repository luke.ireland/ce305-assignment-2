// Generated from /Users/lukire01/Documents/Uni/CE305/ce305-assignment-2/SmallCompiler/src/Compiler.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CompilerParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, NL=2, WS=3, CLASS=4, FOR=5, IN=6, COM=7, DEF=8, BRAC=9, KETS=10, 
		PRINT=11, INC=12, DEC=13, ASSIGN=14, IF=15, ELIF=16, ELSE=17, WHILE=18, 
		MULT=19, DIV=20, ADD=21, SUB=22, MOD=23, COLON=24, SEMICOLON=25, TRUE=26, 
		FALSE=27, NOT=28, AND=29, OR=30, LS=31, LSQ=32, EQU=33, GR=34, GRQ=35, 
		NUM=36, STR=37, VAR=38, INDENT=39, DEDENT=40;
	public static final int
		RULE_start = 0, RULE_stmt = 1, RULE_elifStmt = 2, RULE_elseStmt = 3, RULE_defClass = 4, 
		RULE_defFunc = 5, RULE_func = 6, RULE_expr = 7, RULE_boolExp = 8;
	public static final String[] ruleNames = {
		"start", "stmt", "elifStmt", "elseStmt", "defClass", "defFunc", "func", 
		"expr", "boolExp"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'.'", null, null, "'class'", "'for'", "'in'", "','", "'def'", "'('", 
		"')'", "'print'", "'+='", "'-='", "'='", "'if'", "'elif'", "'else'", "'while'", 
		"'*'", "'/'", "'+'", "'-'", "'%'", "':'", "';'", "'true'", "'false'", 
		"'not'", "'and'", "'or'", "'<'", "'<='", "'=='", "'>'", "'>='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, "NL", "WS", "CLASS", "FOR", "IN", "COM", "DEF", "BRAC", "KETS", 
		"PRINT", "INC", "DEC", "ASSIGN", "IF", "ELIF", "ELSE", "WHILE", "MULT", 
		"DIV", "ADD", "SUB", "MOD", "COLON", "SEMICOLON", "TRUE", "FALSE", "NOT", 
		"AND", "OR", "LS", "LSQ", "EQU", "GR", "GRQ", "NUM", "STR", "VAR", "INDENT", 
		"DEDENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Compiler.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public CompilerParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StartContext extends ParserRuleContext {
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(21);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NL) | (1L << CLASS) | (1L << FOR) | (1L << DEF) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << VAR))) != 0)) {
				{
				{
				setState(18);
				stmt(0);
				}
				}
				setState(23);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
	 
		public StmtContext() { }
		public void copyFrom(StmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OpAssignContext extends StmtContext {
		public Token op;
		public TerminalNode VAR() { return getToken(CompilerParser.VAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode INC() { return getToken(CompilerParser.INC, 0); }
		public TerminalNode DEC() { return getToken(CompilerParser.DEC, 0); }
		public OpAssignContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class NewlineContext extends StmtContext {
		public TerminalNode NL() { return getToken(CompilerParser.NL, 0); }
		public NewlineContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class DefClassStmtContext extends StmtContext {
		public DefClassContext defClass() {
			return getRuleContext(DefClassContext.class,0);
		}
		public DefClassStmtContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class ForContext extends StmtContext {
		public TerminalNode FOR() { return getToken(CompilerParser.FOR, 0); }
		public TerminalNode IN() { return getToken(CompilerParser.IN, 0); }
		public TerminalNode COLON() { return getToken(CompilerParser.COLON, 0); }
		public TerminalNode INDENT() { return getToken(CompilerParser.INDENT, 0); }
		public TerminalNode DEDENT() { return getToken(CompilerParser.DEDENT, 0); }
		public List<TerminalNode> VAR() { return getTokens(CompilerParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(CompilerParser.VAR, i);
		}
		public FuncContext func() {
			return getRuleContext(FuncContext.class,0);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public ForContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class PrintExpContext extends StmtContext {
		public TerminalNode PRINT() { return getToken(CompilerParser.PRINT, 0); }
		public TerminalNode BRAC() { return getToken(CompilerParser.BRAC, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode KETS() { return getToken(CompilerParser.KETS, 0); }
		public PrintExpContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class PrintBoolContext extends StmtContext {
		public TerminalNode PRINT() { return getToken(CompilerParser.PRINT, 0); }
		public TerminalNode BRAC() { return getToken(CompilerParser.BRAC, 0); }
		public BoolExpContext boolExp() {
			return getRuleContext(BoolExpContext.class,0);
		}
		public TerminalNode KETS() { return getToken(CompilerParser.KETS, 0); }
		public PrintBoolContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class WhileContext extends StmtContext {
		public TerminalNode WHILE() { return getToken(CompilerParser.WHILE, 0); }
		public BoolExpContext boolExp() {
			return getRuleContext(BoolExpContext.class,0);
		}
		public TerminalNode COLON() { return getToken(CompilerParser.COLON, 0); }
		public TerminalNode INDENT() { return getToken(CompilerParser.INDENT, 0); }
		public TerminalNode DEDENT() { return getToken(CompilerParser.DEDENT, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public WhileContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class IfContext extends StmtContext {
		public TerminalNode IF() { return getToken(CompilerParser.IF, 0); }
		public BoolExpContext boolExp() {
			return getRuleContext(BoolExpContext.class,0);
		}
		public TerminalNode COLON() { return getToken(CompilerParser.COLON, 0); }
		public TerminalNode INDENT() { return getToken(CompilerParser.INDENT, 0); }
		public TerminalNode DEDENT() { return getToken(CompilerParser.DEDENT, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public ElifStmtContext elifStmt() {
			return getRuleContext(ElifStmtContext.class,0);
		}
		public ElseStmtContext elseStmt() {
			return getRuleContext(ElseStmtContext.class,0);
		}
		public IfContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class CallFuncContext extends StmtContext {
		public FuncContext func() {
			return getRuleContext(FuncContext.class,0);
		}
		public CallFuncContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class CompStmtContext extends StmtContext {
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public TerminalNode SEMICOLON() { return getToken(CompilerParser.SEMICOLON, 0); }
		public CompStmtContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class AssignContext extends StmtContext {
		public TerminalNode VAR() { return getToken(CompilerParser.VAR, 0); }
		public TerminalNode ASSIGN() { return getToken(CompilerParser.ASSIGN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AssignContext(StmtContext ctx) { copyFrom(ctx); }
	}
	public static class DefFuncStmtContext extends StmtContext {
		public DefFuncContext defFunc() {
			return getRuleContext(DefFuncContext.class,0);
		}
		public DefFuncStmtContext(StmtContext ctx) { copyFrom(ctx); }
	}

	public final StmtContext stmt() throws RecognitionException {
		return stmt(0);
	}

	private StmtContext stmt(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		StmtContext _localctx = new StmtContext(_ctx, _parentState);
		StmtContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_stmt, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				{
				_localctx = new OpAssignContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(25);
				match(VAR);
				setState(26);
				((OpAssignContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==INC || _la==DEC) ) {
					((OpAssignContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(27);
				expr(0);
				}
				break;
			case 2:
				{
				_localctx = new AssignContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(28);
				match(VAR);
				setState(29);
				match(ASSIGN);
				setState(30);
				expr(0);
				}
				break;
			case 3:
				{
				_localctx = new DefClassStmtContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(31);
				defClass();
				}
				break;
			case 4:
				{
				_localctx = new DefFuncStmtContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(32);
				defFunc();
				}
				break;
			case 5:
				{
				_localctx = new IfContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(33);
				match(IF);
				setState(34);
				boolExp(0);
				setState(35);
				match(COLON);
				setState(36);
				match(INDENT);
				setState(38); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(37);
					stmt(0);
					}
					}
					setState(40); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NL) | (1L << CLASS) | (1L << FOR) | (1L << DEF) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << VAR))) != 0) );
				setState(42);
				match(DEDENT);
				setState(44);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
				case 1:
					{
					setState(43);
					elifStmt();
					}
					break;
				}
				setState(47);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
				case 1:
					{
					setState(46);
					elseStmt();
					}
					break;
				}
				}
				break;
			case 6:
				{
				_localctx = new WhileContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(49);
				match(WHILE);
				setState(50);
				boolExp(0);
				setState(51);
				match(COLON);
				setState(52);
				match(INDENT);
				setState(54); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(53);
					stmt(0);
					}
					}
					setState(56); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NL) | (1L << CLASS) | (1L << FOR) | (1L << DEF) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << VAR))) != 0) );
				setState(58);
				match(DEDENT);
				}
				break;
			case 7:
				{
				_localctx = new ForContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(60);
				match(FOR);
				setState(62); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(61);
					match(VAR);
					}
					}
					setState(64); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==VAR );
				setState(66);
				match(IN);
				setState(69);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
				case 1:
					{
					setState(67);
					match(VAR);
					}
					break;
				case 2:
					{
					setState(68);
					func();
					}
					break;
				}
				setState(71);
				match(COLON);
				setState(72);
				match(INDENT);
				setState(74); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(73);
					stmt(0);
					}
					}
					setState(76); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NL) | (1L << CLASS) | (1L << FOR) | (1L << DEF) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << VAR))) != 0) );
				setState(78);
				match(DEDENT);
				}
				break;
			case 8:
				{
				_localctx = new PrintExpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(80);
				match(PRINT);
				setState(81);
				match(BRAC);
				setState(82);
				expr(0);
				setState(83);
				match(KETS);
				}
				break;
			case 9:
				{
				_localctx = new PrintBoolContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(85);
				match(PRINT);
				setState(86);
				match(BRAC);
				setState(87);
				boolExp(0);
				setState(88);
				match(KETS);
				}
				break;
			case 10:
				{
				_localctx = new CallFuncContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(90);
				func();
				}
				break;
			case 11:
				{
				_localctx = new NewlineContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(91);
				match(NL);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(99);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new CompStmtContext(new StmtContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_stmt);
					setState(94);
					if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
					setState(95);
					match(SEMICOLON);
					setState(96);
					stmt(11);
					}
					} 
				}
				setState(101);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ElifStmtContext extends ParserRuleContext {
		public TerminalNode ELIF() { return getToken(CompilerParser.ELIF, 0); }
		public BoolExpContext boolExp() {
			return getRuleContext(BoolExpContext.class,0);
		}
		public TerminalNode COLON() { return getToken(CompilerParser.COLON, 0); }
		public TerminalNode INDENT() { return getToken(CompilerParser.INDENT, 0); }
		public TerminalNode DEDENT() { return getToken(CompilerParser.DEDENT, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public ElifStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elifStmt; }
	}

	public final ElifStmtContext elifStmt() throws RecognitionException {
		ElifStmtContext _localctx = new ElifStmtContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_elifStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102);
			match(ELIF);
			setState(103);
			boolExp(0);
			setState(104);
			match(COLON);
			setState(105);
			match(INDENT);
			setState(107); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(106);
				stmt(0);
				}
				}
				setState(109); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NL) | (1L << CLASS) | (1L << FOR) | (1L << DEF) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << VAR))) != 0) );
			setState(111);
			match(DEDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseStmtContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(CompilerParser.ELSE, 0); }
		public TerminalNode COLON() { return getToken(CompilerParser.COLON, 0); }
		public TerminalNode INDENT() { return getToken(CompilerParser.INDENT, 0); }
		public TerminalNode DEDENT() { return getToken(CompilerParser.DEDENT, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public ElseStmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseStmt; }
	}

	public final ElseStmtContext elseStmt() throws RecognitionException {
		ElseStmtContext _localctx = new ElseStmtContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_elseStmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			match(ELSE);
			setState(114);
			match(COLON);
			setState(115);
			match(INDENT);
			setState(117); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(116);
				stmt(0);
				}
				}
				setState(119); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NL) | (1L << CLASS) | (1L << FOR) | (1L << DEF) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << VAR))) != 0) );
			setState(121);
			match(DEDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefClassContext extends ParserRuleContext {
		public DefClassContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defClass; }
	 
		public DefClassContext() { }
		public void copyFrom(DefClassContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class DefineClassContext extends DefClassContext {
		public TerminalNode CLASS() { return getToken(CompilerParser.CLASS, 0); }
		public List<TerminalNode> VAR() { return getTokens(CompilerParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(CompilerParser.VAR, i);
		}
		public TerminalNode COLON() { return getToken(CompilerParser.COLON, 0); }
		public TerminalNode INDENT() { return getToken(CompilerParser.INDENT, 0); }
		public TerminalNode DEDENT() { return getToken(CompilerParser.DEDENT, 0); }
		public TerminalNode BRAC() { return getToken(CompilerParser.BRAC, 0); }
		public TerminalNode KETS() { return getToken(CompilerParser.KETS, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public DefineClassContext(DefClassContext ctx) { copyFrom(ctx); }
	}

	public final DefClassContext defClass() throws RecognitionException {
		DefClassContext _localctx = new DefClassContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_defClass);
		int _la;
		try {
			_localctx = new DefineClassContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			match(CLASS);
			setState(124);
			match(VAR);
			setState(130);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BRAC) {
				{
				setState(125);
				match(BRAC);
				setState(127);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==VAR) {
					{
					setState(126);
					match(VAR);
					}
				}

				setState(129);
				match(KETS);
				}
			}

			setState(132);
			match(COLON);
			setState(133);
			match(INDENT);
			setState(135); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(134);
				stmt(0);
				}
				}
				setState(137); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NL) | (1L << CLASS) | (1L << FOR) | (1L << DEF) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << VAR))) != 0) );
			setState(139);
			match(DEDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefFuncContext extends ParserRuleContext {
		public DefFuncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defFunc; }
	 
		public DefFuncContext() { }
		public void copyFrom(DefFuncContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class DefineFuncContext extends DefFuncContext {
		public TerminalNode DEF() { return getToken(CompilerParser.DEF, 0); }
		public List<TerminalNode> VAR() { return getTokens(CompilerParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(CompilerParser.VAR, i);
		}
		public TerminalNode BRAC() { return getToken(CompilerParser.BRAC, 0); }
		public TerminalNode KETS() { return getToken(CompilerParser.KETS, 0); }
		public TerminalNode COLON() { return getToken(CompilerParser.COLON, 0); }
		public TerminalNode INDENT() { return getToken(CompilerParser.INDENT, 0); }
		public TerminalNode DEDENT() { return getToken(CompilerParser.DEDENT, 0); }
		public List<TerminalNode> COM() { return getTokens(CompilerParser.COM); }
		public TerminalNode COM(int i) {
			return getToken(CompilerParser.COM, i);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public DefineFuncContext(DefFuncContext ctx) { copyFrom(ctx); }
	}

	public final DefFuncContext defFunc() throws RecognitionException {
		DefFuncContext _localctx = new DefFuncContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_defFunc);
		int _la;
		try {
			_localctx = new DefineFuncContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			match(DEF);
			setState(142);
			match(VAR);
			setState(143);
			match(BRAC);
			setState(145);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==VAR) {
				{
				setState(144);
				match(VAR);
				}
			}

			setState(151);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COM) {
				{
				{
				setState(147);
				match(COM);
				setState(148);
				match(VAR);
				}
				}
				setState(153);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(154);
			match(KETS);
			setState(155);
			match(COLON);
			setState(156);
			match(INDENT);
			setState(158); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(157);
				stmt(0);
				}
				}
				setState(160); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NL) | (1L << CLASS) | (1L << FOR) | (1L << DEF) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << VAR))) != 0) );
			setState(162);
			match(DEDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncContext extends ParserRuleContext {
		public FuncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func; }
	 
		public FuncContext() { }
		public void copyFrom(FuncContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FunctionContext extends FuncContext {
		public List<TerminalNode> VAR() { return getTokens(CompilerParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(CompilerParser.VAR, i);
		}
		public TerminalNode BRAC() { return getToken(CompilerParser.BRAC, 0); }
		public TerminalNode KETS() { return getToken(CompilerParser.KETS, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COM() { return getTokens(CompilerParser.COM); }
		public TerminalNode COM(int i) {
			return getToken(CompilerParser.COM, i);
		}
		public FunctionContext(FuncContext ctx) { copyFrom(ctx); }
	}

	public final FuncContext func() throws RecognitionException {
		FuncContext _localctx = new FuncContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_func);
		int _la;
		try {
			_localctx = new FunctionContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(166);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				{
				setState(164);
				match(VAR);
				setState(165);
				match(T__0);
				}
				break;
			}
			setState(168);
			match(VAR);
			setState(169);
			match(BRAC);
			setState(171);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUM) | (1L << STR) | (1L << VAR))) != 0)) {
				{
				setState(170);
				expr(0);
				}
			}

			setState(177);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COM) {
				{
				{
				setState(173);
				match(COM);
				setState(174);
				expr(0);
				}
				}
				setState(179);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(180);
			match(KETS);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NumberContext extends ExprContext {
		public TerminalNode NUM() { return getToken(CompilerParser.NUM, 0); }
		public NumberContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class FuncExprContext extends ExprContext {
		public FuncContext func() {
			return getRuleContext(FuncContext.class,0);
		}
		public FuncExprContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class StringContext extends ExprContext {
		public TerminalNode STR() { return getToken(CompilerParser.STR, 0); }
		public StringContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class VariableContext extends ExprContext {
		public TerminalNode VAR() { return getToken(CompilerParser.VAR, 0); }
		public VariableContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class MultiplicativeContext extends ExprContext {
		public ExprContext left;
		public Token op;
		public ExprContext right;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode MULT() { return getToken(CompilerParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(CompilerParser.DIV, 0); }
		public MultiplicativeContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ModulusContext extends ExprContext {
		public ExprContext left;
		public ExprContext right;
		public TerminalNode MOD() { return getToken(CompilerParser.MOD, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ModulusContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class AdditiveContext extends ExprContext {
		public ExprContext left;
		public Token op;
		public ExprContext right;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ADD() { return getToken(CompilerParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(CompilerParser.SUB, 0); }
		public AdditiveContext(ExprContext ctx) { copyFrom(ctx); }
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 14;
		enterRecursionRule(_localctx, 14, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(187);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				_localctx = new FuncExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(183);
				func();
				}
				break;
			case 2:
				{
				_localctx = new NumberContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(184);
				match(NUM);
				}
				break;
			case 3:
				{
				_localctx = new StringContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(185);
				match(STR);
				}
				break;
			case 4:
				{
				_localctx = new VariableContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(186);
				match(VAR);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(200);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(198);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
					case 1:
						{
						_localctx = new MultiplicativeContext(new ExprContext(_parentctx, _parentState));
						((MultiplicativeContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(189);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(190);
						((MultiplicativeContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==MULT || _la==DIV) ) {
							((MultiplicativeContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(191);
						((MultiplicativeContext)_localctx).right = expr(7);
						}
						break;
					case 2:
						{
						_localctx = new AdditiveContext(new ExprContext(_parentctx, _parentState));
						((AdditiveContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(192);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(193);
						((AdditiveContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==ADD || _la==SUB) ) {
							((AdditiveContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(194);
						((AdditiveContext)_localctx).right = expr(6);
						}
						break;
					case 3:
						{
						_localctx = new ModulusContext(new ExprContext(_parentctx, _parentState));
						((ModulusContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(195);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(196);
						match(MOD);
						setState(197);
						((ModulusContext)_localctx).right = expr(5);
						}
						break;
					}
					} 
				}
				setState(202);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BoolExpContext extends ParserRuleContext {
		public BoolExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolExp; }
	 
		public BoolExpContext() { }
		public void copyFrom(BoolExpContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BoolOpContext extends BoolExpContext {
		public BoolExpContext left;
		public Token op;
		public BoolExpContext right;
		public List<BoolExpContext> boolExp() {
			return getRuleContexts(BoolExpContext.class);
		}
		public BoolExpContext boolExp(int i) {
			return getRuleContext(BoolExpContext.class,i);
		}
		public TerminalNode AND() { return getToken(CompilerParser.AND, 0); }
		public TerminalNode OR() { return getToken(CompilerParser.OR, 0); }
		public BoolOpContext(BoolExpContext ctx) { copyFrom(ctx); }
	}
	public static class TrueContext extends BoolExpContext {
		public TerminalNode TRUE() { return getToken(CompilerParser.TRUE, 0); }
		public TrueContext(BoolExpContext ctx) { copyFrom(ctx); }
	}
	public static class FalseContext extends BoolExpContext {
		public TerminalNode FALSE() { return getToken(CompilerParser.FALSE, 0); }
		public FalseContext(BoolExpContext ctx) { copyFrom(ctx); }
	}
	public static class NegBoolContext extends BoolExpContext {
		public TerminalNode NOT() { return getToken(CompilerParser.NOT, 0); }
		public BoolExpContext boolExp() {
			return getRuleContext(BoolExpContext.class,0);
		}
		public NegBoolContext(BoolExpContext ctx) { copyFrom(ctx); }
	}
	public static class RelationalContext extends BoolExpContext {
		public ExprContext left;
		public Token op;
		public ExprContext right;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode LS() { return getToken(CompilerParser.LS, 0); }
		public TerminalNode LSQ() { return getToken(CompilerParser.LSQ, 0); }
		public TerminalNode EQU() { return getToken(CompilerParser.EQU, 0); }
		public TerminalNode GR() { return getToken(CompilerParser.GR, 0); }
		public TerminalNode GRQ() { return getToken(CompilerParser.GRQ, 0); }
		public RelationalContext(BoolExpContext ctx) { copyFrom(ctx); }
	}

	public final BoolExpContext boolExp() throws RecognitionException {
		return boolExp(0);
	}

	private BoolExpContext boolExp(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BoolExpContext _localctx = new BoolExpContext(_ctx, _parentState);
		BoolExpContext _prevctx = _localctx;
		int _startState = 16;
		enterRecursionRule(_localctx, 16, RULE_boolExp, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUE:
				{
				_localctx = new TrueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(204);
				match(TRUE);
				}
				break;
			case FALSE:
				{
				_localctx = new FalseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(205);
				match(FALSE);
				}
				break;
			case NOT:
				{
				_localctx = new NegBoolContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(206);
				match(NOT);
				setState(207);
				boolExp(3);
				}
				break;
			case NUM:
			case STR:
			case VAR:
				{
				_localctx = new RelationalContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(208);
				((RelationalContext)_localctx).left = expr(0);
				setState(209);
				((RelationalContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LS) | (1L << LSQ) | (1L << EQU) | (1L << GR) | (1L << GRQ))) != 0)) ) {
					((RelationalContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(210);
				((RelationalContext)_localctx).right = expr(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(219);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new BoolOpContext(new BoolExpContext(_parentctx, _parentState));
					((BoolOpContext)_localctx).left = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_boolExp);
					setState(214);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(215);
					((BoolOpContext)_localctx).op = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==AND || _la==OR) ) {
						((BoolOpContext)_localctx).op = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(216);
					((BoolOpContext)_localctx).right = boolExp(3);
					}
					} 
				}
				setState(221);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return stmt_sempred((StmtContext)_localctx, predIndex);
		case 7:
			return expr_sempred((ExprContext)_localctx, predIndex);
		case 8:
			return boolExp_sempred((BoolExpContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean stmt_sempred(StmtContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 10);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 6);
		case 2:
			return precpred(_ctx, 5);
		case 3:
			return precpred(_ctx, 4);
		}
		return true;
	}
	private boolean boolExp_sempred(BoolExpContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3*\u00e1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\7\2"+
		"\26\n\2\f\2\16\2\31\13\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\6\3)\n\3\r\3\16\3*\3\3\3\3\5\3/\n\3\3\3\5\3\62\n\3\3\3\3\3"+
		"\3\3\3\3\3\3\6\39\n\3\r\3\16\3:\3\3\3\3\3\3\3\3\6\3A\n\3\r\3\16\3B\3\3"+
		"\3\3\3\3\5\3H\n\3\3\3\3\3\3\3\6\3M\n\3\r\3\16\3N\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3_\n\3\3\3\3\3\3\3\7\3d\n\3\f\3\16"+
		"\3g\13\3\3\4\3\4\3\4\3\4\3\4\6\4n\n\4\r\4\16\4o\3\4\3\4\3\5\3\5\3\5\3"+
		"\5\6\5x\n\5\r\5\16\5y\3\5\3\5\3\6\3\6\3\6\3\6\5\6\u0082\n\6\3\6\5\6\u0085"+
		"\n\6\3\6\3\6\3\6\6\6\u008a\n\6\r\6\16\6\u008b\3\6\3\6\3\7\3\7\3\7\3\7"+
		"\5\7\u0094\n\7\3\7\3\7\7\7\u0098\n\7\f\7\16\7\u009b\13\7\3\7\3\7\3\7\3"+
		"\7\6\7\u00a1\n\7\r\7\16\7\u00a2\3\7\3\7\3\b\3\b\5\b\u00a9\n\b\3\b\3\b"+
		"\3\b\5\b\u00ae\n\b\3\b\3\b\7\b\u00b2\n\b\f\b\16\b\u00b5\13\b\3\b\3\b\3"+
		"\t\3\t\3\t\3\t\3\t\5\t\u00be\n\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7"+
		"\t\u00c9\n\t\f\t\16\t\u00cc\13\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5"+
		"\n\u00d7\n\n\3\n\3\n\3\n\7\n\u00dc\n\n\f\n\16\n\u00df\13\n\3\n\2\5\4\20"+
		"\22\13\2\4\6\b\n\f\16\20\22\2\7\3\2\16\17\3\2\25\26\3\2\27\30\3\2!%\3"+
		"\2\37 \2\u00ff\2\27\3\2\2\2\4^\3\2\2\2\6h\3\2\2\2\bs\3\2\2\2\n}\3\2\2"+
		"\2\f\u008f\3\2\2\2\16\u00a8\3\2\2\2\20\u00bd\3\2\2\2\22\u00d6\3\2\2\2"+
		"\24\26\5\4\3\2\25\24\3\2\2\2\26\31\3\2\2\2\27\25\3\2\2\2\27\30\3\2\2\2"+
		"\30\3\3\2\2\2\31\27\3\2\2\2\32\33\b\3\1\2\33\34\7(\2\2\34\35\t\2\2\2\35"+
		"_\5\20\t\2\36\37\7(\2\2\37 \7\20\2\2 _\5\20\t\2!_\5\n\6\2\"_\5\f\7\2#"+
		"$\7\21\2\2$%\5\22\n\2%&\7\32\2\2&(\7)\2\2\')\5\4\3\2(\'\3\2\2\2)*\3\2"+
		"\2\2*(\3\2\2\2*+\3\2\2\2+,\3\2\2\2,.\7*\2\2-/\5\6\4\2.-\3\2\2\2./\3\2"+
		"\2\2/\61\3\2\2\2\60\62\5\b\5\2\61\60\3\2\2\2\61\62\3\2\2\2\62_\3\2\2\2"+
		"\63\64\7\24\2\2\64\65\5\22\n\2\65\66\7\32\2\2\668\7)\2\2\679\5\4\3\28"+
		"\67\3\2\2\29:\3\2\2\2:8\3\2\2\2:;\3\2\2\2;<\3\2\2\2<=\7*\2\2=_\3\2\2\2"+
		">@\7\7\2\2?A\7(\2\2@?\3\2\2\2AB\3\2\2\2B@\3\2\2\2BC\3\2\2\2CD\3\2\2\2"+
		"DG\7\b\2\2EH\7(\2\2FH\5\16\b\2GE\3\2\2\2GF\3\2\2\2HI\3\2\2\2IJ\7\32\2"+
		"\2JL\7)\2\2KM\5\4\3\2LK\3\2\2\2MN\3\2\2\2NL\3\2\2\2NO\3\2\2\2OP\3\2\2"+
		"\2PQ\7*\2\2Q_\3\2\2\2RS\7\r\2\2ST\7\13\2\2TU\5\20\t\2UV\7\f\2\2V_\3\2"+
		"\2\2WX\7\r\2\2XY\7\13\2\2YZ\5\22\n\2Z[\7\f\2\2[_\3\2\2\2\\_\5\16\b\2]"+
		"_\7\4\2\2^\32\3\2\2\2^\36\3\2\2\2^!\3\2\2\2^\"\3\2\2\2^#\3\2\2\2^\63\3"+
		"\2\2\2^>\3\2\2\2^R\3\2\2\2^W\3\2\2\2^\\\3\2\2\2^]\3\2\2\2_e\3\2\2\2`a"+
		"\f\f\2\2ab\7\33\2\2bd\5\4\3\rc`\3\2\2\2dg\3\2\2\2ec\3\2\2\2ef\3\2\2\2"+
		"f\5\3\2\2\2ge\3\2\2\2hi\7\22\2\2ij\5\22\n\2jk\7\32\2\2km\7)\2\2ln\5\4"+
		"\3\2ml\3\2\2\2no\3\2\2\2om\3\2\2\2op\3\2\2\2pq\3\2\2\2qr\7*\2\2r\7\3\2"+
		"\2\2st\7\23\2\2tu\7\32\2\2uw\7)\2\2vx\5\4\3\2wv\3\2\2\2xy\3\2\2\2yw\3"+
		"\2\2\2yz\3\2\2\2z{\3\2\2\2{|\7*\2\2|\t\3\2\2\2}~\7\6\2\2~\u0084\7(\2\2"+
		"\177\u0081\7\13\2\2\u0080\u0082\7(\2\2\u0081\u0080\3\2\2\2\u0081\u0082"+
		"\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0085\7\f\2\2\u0084\177\3\2\2\2\u0084"+
		"\u0085\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087\7\32\2\2\u0087\u0089\7"+
		")\2\2\u0088\u008a\5\4\3\2\u0089\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b"+
		"\u0089\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u008e\7*"+
		"\2\2\u008e\13\3\2\2\2\u008f\u0090\7\n\2\2\u0090\u0091\7(\2\2\u0091\u0093"+
		"\7\13\2\2\u0092\u0094\7(\2\2\u0093\u0092\3\2\2\2\u0093\u0094\3\2\2\2\u0094"+
		"\u0099\3\2\2\2\u0095\u0096\7\t\2\2\u0096\u0098\7(\2\2\u0097\u0095\3\2"+
		"\2\2\u0098\u009b\3\2\2\2\u0099\u0097\3\2\2\2\u0099\u009a\3\2\2\2\u009a"+
		"\u009c\3\2\2\2\u009b\u0099\3\2\2\2\u009c\u009d\7\f\2\2\u009d\u009e\7\32"+
		"\2\2\u009e\u00a0\7)\2\2\u009f\u00a1\5\4\3\2\u00a0\u009f\3\2\2\2\u00a1"+
		"\u00a2\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a4\3\2"+
		"\2\2\u00a4\u00a5\7*\2\2\u00a5\r\3\2\2\2\u00a6\u00a7\7(\2\2\u00a7\u00a9"+
		"\7\3\2\2\u00a8\u00a6\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa"+
		"\u00ab\7(\2\2\u00ab\u00ad\7\13\2\2\u00ac\u00ae\5\20\t\2\u00ad\u00ac\3"+
		"\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00b3\3\2\2\2\u00af\u00b0\7\t\2\2\u00b0"+
		"\u00b2\5\20\t\2\u00b1\u00af\3\2\2\2\u00b2\u00b5\3\2\2\2\u00b3\u00b1\3"+
		"\2\2\2\u00b3\u00b4\3\2\2\2\u00b4\u00b6\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b6"+
		"\u00b7\7\f\2\2\u00b7\17\3\2\2\2\u00b8\u00b9\b\t\1\2\u00b9\u00be\5\16\b"+
		"\2\u00ba\u00be\7&\2\2\u00bb\u00be\7\'\2\2\u00bc\u00be\7(\2\2\u00bd\u00b8"+
		"\3\2\2\2\u00bd\u00ba\3\2\2\2\u00bd\u00bb\3\2\2\2\u00bd\u00bc\3\2\2\2\u00be"+
		"\u00ca\3\2\2\2\u00bf\u00c0\f\b\2\2\u00c0\u00c1\t\3\2\2\u00c1\u00c9\5\20"+
		"\t\t\u00c2\u00c3\f\7\2\2\u00c3\u00c4\t\4\2\2\u00c4\u00c9\5\20\t\b\u00c5"+
		"\u00c6\f\6\2\2\u00c6\u00c7\7\31\2\2\u00c7\u00c9\5\20\t\7\u00c8\u00bf\3"+
		"\2\2\2\u00c8\u00c2\3\2\2\2\u00c8\u00c5\3\2\2\2\u00c9\u00cc\3\2\2\2\u00ca"+
		"\u00c8\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb\21\3\2\2\2\u00cc\u00ca\3\2\2"+
		"\2\u00cd\u00ce\b\n\1\2\u00ce\u00d7\7\34\2\2\u00cf\u00d7\7\35\2\2\u00d0"+
		"\u00d1\7\36\2\2\u00d1\u00d7\5\22\n\5\u00d2\u00d3\5\20\t\2\u00d3\u00d4"+
		"\t\5\2\2\u00d4\u00d5\5\20\t\2\u00d5\u00d7\3\2\2\2\u00d6\u00cd\3\2\2\2"+
		"\u00d6\u00cf\3\2\2\2\u00d6\u00d0\3\2\2\2\u00d6\u00d2\3\2\2\2\u00d7\u00dd"+
		"\3\2\2\2\u00d8\u00d9\f\4\2\2\u00d9\u00da\t\6\2\2\u00da\u00dc\5\22\n\5"+
		"\u00db\u00d8\3\2\2\2\u00dc\u00df\3\2\2\2\u00dd\u00db\3\2\2\2\u00dd\u00de"+
		"\3\2\2\2\u00de\23\3\2\2\2\u00df\u00dd\3\2\2\2\34\27*.\61:BGN^eoy\u0081"+
		"\u0084\u008b\u0093\u0099\u00a2\u00a8\u00ad\u00b3\u00bd\u00c8\u00ca\u00d6"+
		"\u00dd";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}