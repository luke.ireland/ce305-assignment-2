// Generated from /Users/lukire01/Documents/Uni/CE305/ce305-assignment-2/SmallCompiler/src/Compiler.g4 by ANTLR 4.7.1

  import com.yuvalshavit.antlr4.DenterHelper;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CompilerLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, NL=2, WS=3, CLASS=4, FOR=5, IN=6, COM=7, DEF=8, BRAC=9, KETS=10, 
		PRINT=11, INC=12, DEC=13, ASSIGN=14, IF=15, ELIF=16, ELSE=17, WHILE=18, 
		MULT=19, DIV=20, ADD=21, SUB=22, MOD=23, COLON=24, SEMICOLON=25, TRUE=26, 
		FALSE=27, NOT=28, AND=29, OR=30, LS=31, LSQ=32, EQU=33, GR=34, GRQ=35, 
		NUM=36, STR=37, VAR=38;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "NL", "WS", "CLASS", "FOR", "IN", "COM", "DEF", "BRAC", "KETS", 
		"PRINT", "INC", "DEC", "ASSIGN", "IF", "ELIF", "ELSE", "WHILE", "MULT", 
		"DIV", "ADD", "SUB", "MOD", "COLON", "SEMICOLON", "TRUE", "FALSE", "NOT", 
		"AND", "OR", "LS", "LSQ", "EQU", "GR", "GRQ", "NUM", "STR", "VAR"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'.'", null, null, "'class'", "'for'", "'in'", "','", "'def'", "'('", 
		"')'", "'print'", "'+='", "'-='", "'='", "'if'", "'elif'", "'else'", "'while'", 
		"'*'", "'/'", "'+'", "'-'", "'%'", "':'", "';'", "'true'", "'false'", 
		"'not'", "'and'", "'or'", "'<'", "'<='", "'=='", "'>'", "'>='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, "NL", "WS", "CLASS", "FOR", "IN", "COM", "DEF", "BRAC", "KETS", 
		"PRINT", "INC", "DEC", "ASSIGN", "IF", "ELIF", "ELSE", "WHILE", "MULT", 
		"DIV", "ADD", "SUB", "MOD", "COLON", "SEMICOLON", "TRUE", "FALSE", "NOT", 
		"AND", "OR", "LS", "LSQ", "EQU", "GR", "GRQ", "NUM", "STR", "VAR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	  private final DenterHelper denter = new DenterHelper(NL,
	                                                       CompilerParser.INDENT,
	                                                       CompilerParser.DEDENT)
	  {
	    @Override
	    public Token pullToken() {
	      return CompilerLexer.super.nextToken();
	    }
	  };

	  @Override
	  public Token nextToken() {
	    return denter.nextToken();
	  }


	public CompilerLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Compiler.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2(\u00e9\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\3\2\3\2\3\3\5\3S\n\3\3\3\3"+
		"\3\7\3W\n\3\f\3\16\3Z\13\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6"+
		"\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\13\3\13\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16\3\17\3\17\3\20\3\20\3"+
		"\20\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3"+
		"\23\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3"+
		"\31\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3"+
		"\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3 \3 \3!\3!\3!\3"+
		"\"\3\"\3\"\3#\3#\3$\3$\3$\3%\5%\u00ca\n%\3%\6%\u00cd\n%\r%\16%\u00ce\3"+
		"%\3%\6%\u00d3\n%\r%\16%\u00d4\5%\u00d7\n%\3&\3&\3&\3&\3&\3&\3&\3&\5&\u00e1"+
		"\n&\3\'\3\'\7\'\u00e5\n\'\f\'\16\'\u00e8\13\'\2\2(\3\3\5\4\7\5\t\6\13"+
		"\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'"+
		"\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'"+
		"M(\3\2\6\4\2\13\13\"\"\3\2\62;\4\2C\\c|\5\2\62;C\\c|\2\u00f0\2\3\3\2\2"+
		"\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3"+
		"\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2"+
		"\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2"+
		"\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2"+
		"\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3"+
		"\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2"+
		"\2\2K\3\2\2\2\2M\3\2\2\2\3O\3\2\2\2\5R\3\2\2\2\7[\3\2\2\2\t_\3\2\2\2\13"+
		"e\3\2\2\2\ri\3\2\2\2\17l\3\2\2\2\21n\3\2\2\2\23r\3\2\2\2\25t\3\2\2\2\27"+
		"v\3\2\2\2\31|\3\2\2\2\33\177\3\2\2\2\35\u0082\3\2\2\2\37\u0084\3\2\2\2"+
		"!\u0087\3\2\2\2#\u008c\3\2\2\2%\u0091\3\2\2\2\'\u0097\3\2\2\2)\u0099\3"+
		"\2\2\2+\u009b\3\2\2\2-\u009d\3\2\2\2/\u009f\3\2\2\2\61\u00a1\3\2\2\2\63"+
		"\u00a3\3\2\2\2\65\u00a5\3\2\2\2\67\u00aa\3\2\2\29\u00b0\3\2\2\2;\u00b4"+
		"\3\2\2\2=\u00b8\3\2\2\2?\u00bb\3\2\2\2A\u00bd\3\2\2\2C\u00c0\3\2\2\2E"+
		"\u00c3\3\2\2\2G\u00c5\3\2\2\2I\u00c9\3\2\2\2K\u00e0\3\2\2\2M\u00e2\3\2"+
		"\2\2OP\7\60\2\2P\4\3\2\2\2QS\7\17\2\2RQ\3\2\2\2RS\3\2\2\2ST\3\2\2\2TX"+
		"\7\f\2\2UW\7\"\2\2VU\3\2\2\2WZ\3\2\2\2XV\3\2\2\2XY\3\2\2\2Y\6\3\2\2\2"+
		"ZX\3\2\2\2[\\\t\2\2\2\\]\3\2\2\2]^\b\4\2\2^\b\3\2\2\2_`\7e\2\2`a\7n\2"+
		"\2ab\7c\2\2bc\7u\2\2cd\7u\2\2d\n\3\2\2\2ef\7h\2\2fg\7q\2\2gh\7t\2\2h\f"+
		"\3\2\2\2ij\7k\2\2jk\7p\2\2k\16\3\2\2\2lm\7.\2\2m\20\3\2\2\2no\7f\2\2o"+
		"p\7g\2\2pq\7h\2\2q\22\3\2\2\2rs\7*\2\2s\24\3\2\2\2tu\7+\2\2u\26\3\2\2"+
		"\2vw\7r\2\2wx\7t\2\2xy\7k\2\2yz\7p\2\2z{\7v\2\2{\30\3\2\2\2|}\7-\2\2}"+
		"~\7?\2\2~\32\3\2\2\2\177\u0080\7/\2\2\u0080\u0081\7?\2\2\u0081\34\3\2"+
		"\2\2\u0082\u0083\7?\2\2\u0083\36\3\2\2\2\u0084\u0085\7k\2\2\u0085\u0086"+
		"\7h\2\2\u0086 \3\2\2\2\u0087\u0088\7g\2\2\u0088\u0089\7n\2\2\u0089\u008a"+
		"\7k\2\2\u008a\u008b\7h\2\2\u008b\"\3\2\2\2\u008c\u008d\7g\2\2\u008d\u008e"+
		"\7n\2\2\u008e\u008f\7u\2\2\u008f\u0090\7g\2\2\u0090$\3\2\2\2\u0091\u0092"+
		"\7y\2\2\u0092\u0093\7j\2\2\u0093\u0094\7k\2\2\u0094\u0095\7n\2\2\u0095"+
		"\u0096\7g\2\2\u0096&\3\2\2\2\u0097\u0098\7,\2\2\u0098(\3\2\2\2\u0099\u009a"+
		"\7\61\2\2\u009a*\3\2\2\2\u009b\u009c\7-\2\2\u009c,\3\2\2\2\u009d\u009e"+
		"\7/\2\2\u009e.\3\2\2\2\u009f\u00a0\7\'\2\2\u00a0\60\3\2\2\2\u00a1\u00a2"+
		"\7<\2\2\u00a2\62\3\2\2\2\u00a3\u00a4\7=\2\2\u00a4\64\3\2\2\2\u00a5\u00a6"+
		"\7v\2\2\u00a6\u00a7\7t\2\2\u00a7\u00a8\7w\2\2\u00a8\u00a9\7g\2\2\u00a9"+
		"\66\3\2\2\2\u00aa\u00ab\7h\2\2\u00ab\u00ac\7c\2\2\u00ac\u00ad\7n\2\2\u00ad"+
		"\u00ae\7u\2\2\u00ae\u00af\7g\2\2\u00af8\3\2\2\2\u00b0\u00b1\7p\2\2\u00b1"+
		"\u00b2\7q\2\2\u00b2\u00b3\7v\2\2\u00b3:\3\2\2\2\u00b4\u00b5\7c\2\2\u00b5"+
		"\u00b6\7p\2\2\u00b6\u00b7\7f\2\2\u00b7<\3\2\2\2\u00b8\u00b9\7q\2\2\u00b9"+
		"\u00ba\7t\2\2\u00ba>\3\2\2\2\u00bb\u00bc\7>\2\2\u00bc@\3\2\2\2\u00bd\u00be"+
		"\7>\2\2\u00be\u00bf\7?\2\2\u00bfB\3\2\2\2\u00c0\u00c1\7?\2\2\u00c1\u00c2"+
		"\7?\2\2\u00c2D\3\2\2\2\u00c3\u00c4\7@\2\2\u00c4F\3\2\2\2\u00c5\u00c6\7"+
		"@\2\2\u00c6\u00c7\7?\2\2\u00c7H\3\2\2\2\u00c8\u00ca\5-\27\2\u00c9\u00c8"+
		"\3\2\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00cc\3\2\2\2\u00cb\u00cd\t\3\2\2\u00cc"+
		"\u00cb\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce\u00cc\3\2\2\2\u00ce\u00cf\3\2"+
		"\2\2\u00cf\u00d6\3\2\2\2\u00d0\u00d2\7\60\2\2\u00d1\u00d3\t\3\2\2\u00d2"+
		"\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d4\u00d5\3\2"+
		"\2\2\u00d5\u00d7\3\2\2\2\u00d6\u00d0\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7"+
		"J\3\2\2\2\u00d8\u00d9\7$\2\2\u00d9\u00da\5M\'\2\u00da\u00db\7$\2\2\u00db"+
		"\u00e1\3\2\2\2\u00dc\u00dd\7)\2\2\u00dd\u00de\5M\'\2\u00de\u00df\7)\2"+
		"\2\u00df\u00e1\3\2\2\2\u00e0\u00d8\3\2\2\2\u00e0\u00dc\3\2\2\2\u00e1L"+
		"\3\2\2\2\u00e2\u00e6\t\4\2\2\u00e3\u00e5\t\5\2\2\u00e4\u00e3\3\2\2\2\u00e5"+
		"\u00e8\3\2\2\2\u00e6\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7N\3\2\2\2"+
		"\u00e8\u00e6\3\2\2\2\13\2RX\u00c9\u00ce\u00d4\u00d6\u00e0\u00e6\3\b\2"+
		"\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}