package ce305;

import org.antlr.v4.runtime.ParserRuleContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PprintVisitor extends CompilerBaseVisitor<String> {

    @Override
    public String visitStart(CompilerParser.StartContext ctx) {
        List<String> lines = new ArrayList<>();
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            String child = this.visit(stmt);
            if (child != null) {
                lines.addAll(Arrays.asList(child.split("[\\r\\n]+")));
            }
        }
        String result = String.join("\n", lines);
        return result;
    }

    @Override
    public String visitOpAssign(CompilerParser.OpAssignContext ctx) {
        String var = ctx.VAR().getText();
        String op = ctx.op.getText();
        String value = this.visit(ctx.expr());
        String result = String.format("%s %s %s\n", var, op, value);
        if (!ctx.getParent().getTokens(CompilerParser.INDENT).isEmpty()) {
            return "\t" + result;
        }
        return result;
    }

    // Print variable and value of assignment
    @Override
    public String visitAssign(CompilerParser.AssignContext ctx) {
        String var = ctx.VAR().getText();
        String value = this.visit(ctx.expr());
        value = value.strip();
        String result = var + " = " + value + "\n";
        if (!ctx.getParent().getTokens(CompilerParser.INDENT).isEmpty()) {
            return "\t" + result;
        }
        return result;
    }

    @Override
    public String visitCompStmt(CompilerParser.CompStmtContext ctx) {
        String result = "";
        result += this.visit(ctx.stmt(0)) + ";\n";
        result += this.visit(ctx.stmt(1));
        if (!ctx.getParent().getTokens(CompilerParser.INDENT).isEmpty()) {
            return "\t" + result;
        }
        return result;
    }

    @Override
    public String visitIfStmt(CompilerParser.IfStmtContext ctx) {
        List<String> lines = new ArrayList<>();
        boolean indent = false;
        ParserRuleContext parent = ctx.getParent();
        while (parent.getRuleIndex() == CompilerParser.RULE_stmt) {
            parent = parent.getParent();
        }
        if (!parent.getTokens(CompilerParser.INDENT).isEmpty()) {
            indent = true;
        }
        lines.add(String.format("if %s:\n", this.visit(ctx.boolExp())));
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            String child = this.visit(stmt);
            if (child != null) {
                lines.addAll(Arrays.asList(child.split("[\\r\\n]+")));
            }
        }
        if (ctx.elifStmt() != null) {
            String child = this.visit(ctx.elifStmt());
            if (child != null) {
                lines.addAll(Arrays.asList(child.split("[\\r\\n]+")));
            }
        }
        if (ctx.elseStmt() != null) {
            String child = this.visit(ctx.elseStmt());
            if (child != null) {
                lines.addAll(Arrays.asList(child.split("[\\r\\n]+")));
            }
        }
        if (indent) {
            lines.replaceAll(new Tabber());
        }
        String result = String.join("\n", lines);
        return result;
    }

    @Override
    public String visitElifStmt(CompilerParser.ElifStmtContext ctx) {
        String result = String.format("elif %s:\n", this.visit(ctx.boolExp()));
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            String child = this.visit(stmt);
            if (child != null) {
                result += child;
            }
        }
        return result;
    }

    @Override
    public String visitElseStmt(CompilerParser.ElseStmtContext ctx) {
        String result = "else:\n";
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            String child = this.visit(stmt);
            if (child != null) {
                result += child;
            }
        }
        return result;
    }

    @Override
    public String visitDefineClass(CompilerParser.DefineClassContext ctx) {
        List<String> lines = new ArrayList<>();
        boolean indent = false;
        ParserRuleContext parent = ctx.getParent();
        while (parent.getRuleIndex() == CompilerParser.RULE_stmt) {
            parent = parent.getParent();
        }
        if (!parent.getTokens(CompilerParser.INDENT).isEmpty()) {
            indent = true;
        }
        String name = ctx.VAR(0).getText();
        if (ctx.VAR().size() > 1) {
            lines.add(String.format("class %s(%s):", name, ctx.VAR(1).getText()));
        } else {
            lines.add(String.format("class %s:", name));
        }
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            String child = this.visit(stmt);
            if (child != null) {
                lines.addAll(Arrays.asList(child.split("[\\r\\n]+")));
            }
        }
        if (indent) {
            lines.replaceAll(new Tabber());
        }
        String result = String.join("\n", lines);
        return result;
    }

    @Override
    public String visitDefineFunc(CompilerParser.DefineFuncContext ctx) {
        List<String> lines = new ArrayList<>();
        boolean indent = false;
        ParserRuleContext parent = ctx.getParent();
        while (parent.getRuleIndex() == CompilerParser.RULE_stmt) {
            parent = parent.getParent();
        }
        if (!parent.getTokens(CompilerParser.INDENT).isEmpty()) {
            indent = true;
        }
        String name = ctx.VAR(0).getText();
        List<String> argsList = new ArrayList<>();
        for (int i = 1; i < ctx.VAR().size(); i++) {
            argsList.add(ctx.VAR(i).getText());
        }
        String args = String.join(",", argsList);
        lines.add(String.format("def %s(%s):", name, args));
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            String child = this.visit(stmt);
            if (child != null) {
                lines.addAll(Arrays.asList(child.split("[\\r\\n]+")));
            }
        }
        if (indent) {
            lines.replaceAll(new Tabber());
        }
        String result = String.join("\n", lines);
        return result;
    }

    @Override
    public String visitFunction(CompilerParser.FunctionContext ctx) {
        List<String> argsList = new ArrayList<>();
        for (int i = 0; i < ctx.expr().size(); i++) {
            argsList.add(ctx.expr(i).getText());
        }
        String args = String.join(",", argsList);
        int varCount = ctx.VAR().size();
        String funcName = "";
        if (varCount > 1) {
            funcName = ctx.VAR(0).getText() + "." + ctx.VAR(1).getText();
        } else {
            funcName = ctx.VAR(0).getText();
        }
        String result = String.format("%s(%s)\n", funcName, args);
        if (!ctx.getParent().getTokens(CompilerParser.INDENT).isEmpty()) {
            return "\t" + result;
        }
        return result;
    }

    @Override
    public String visitReturn(CompilerParser.ReturnContext ctx){
        return String.format("\treturn %s\n", this.visit(ctx.expr()));
    }

    @Override
    public String visitWhileStmt(CompilerParser.WhileStmtContext ctx) {
        List<String> lines = new ArrayList<>();
        boolean indent = false;
        ParserRuleContext parent = ctx.getParent();
        while (parent.getRuleIndex() == CompilerParser.RULE_stmt) {
            parent = parent.getParent();
        }
        if (!parent.getTokens(CompilerParser.INDENT).isEmpty()) {
            indent = true;
        }
        lines.add(String.format("while %s:", this.visit(ctx.boolExp())));
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            String child = this.visit(stmt);
            if (child != null)
                lines.addAll(Arrays.asList(child.split("[\\r\\n]+")));
        }
        if (indent) {
            lines.replaceAll(new Tabber());
        }
        String result = String.join("\n", lines);
        return result;
    }

    @Override
    public String visitForStmt(CompilerParser.ForStmtContext ctx) {
        List<String> lines = new ArrayList<>();
        boolean indent = false;
        ParserRuleContext parent = ctx.getParent();
        while (parent.getRuleIndex() == CompilerParser.RULE_stmt) {
            parent = parent.getParent();
        }
        if (!parent.getTokens(CompilerParser.INDENT).isEmpty()) {
            indent = true;
        }
        int size = ctx.VAR().size();
        List<String> argsList = new ArrayList<>();
        for (int i = 0; i < ctx.VAR().size() - 1; i++) {
            argsList.add(ctx.VAR(i).getText());
        }
        String args = String.join(",", argsList);
        lines.add(String.format("for %s in %s:", args, ctx.VAR(size - 1).getText()));
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            String child = this.visit(stmt);
            if (child != null) {
                lines.addAll(Arrays.asList(child.split("[\\r\\n]+")));
            }
        }
        if (indent) {
            lines.replaceAll(new Tabber());
        }
        String result = String.join("\n", lines);
        return result;
    }

    @Override
    public String visitString(CompilerParser.StringContext ctx) {
        return ctx.STR().getText();

    }

    @Override
    public String visitTrue(CompilerParser.TrueContext ctx) {
        return "True";
    }

    @Override
    public String visitFalse(CompilerParser.FalseContext ctx) {
        return "False";
    }

    @Override
    public String visitNegBool(CompilerParser.NegBoolContext ctx) {
        return "not " + this.visit(ctx.boolExp());
    }

    @Override
    public String visitBoolOp(CompilerParser.BoolOpContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        String result = left + " " + ctx.op.getText() + " " + right;
        return result;
    }

    @Override
    public String visitRelational(CompilerParser.RelationalContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        String result = left + " " + ctx.op.getText() + " " + right;
        return result;
    }

    @Override
    public String visitModulus(CompilerParser.ModulusContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        String result = left + " % " + right;
        return result;
    }

    @Override
    public String visitExponential(CompilerParser.ExponentialContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        String result = left + " " + ctx.op.getText() + " " + right;
        return result;
    }

    @Override
    public String visitMultiplicative(CompilerParser.MultiplicativeContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        String result = left + " " + ctx.op.getText() + " " + right;
        return result;
    }

    @Override
    public String visitAdditive(CompilerParser.AdditiveContext ctx) {
        String left = this.visit(ctx.left);
        String right = this.visit(ctx.right);
        String result = left + " " + ctx.op.getText() + " " + right;
        return result;
    }

    @Override
    public String visitVariable(CompilerParser.VariableContext ctx) {
        String result = ctx.VAR().getText();
        return result;
    }

    @Override
    public String visitNumber(CompilerParser.NumberContext ctx) {
        String result = ctx.NUM().getText();
        return result;
    }
}
