package ce305;

import org.antlr.v4.runtime.tree.Tree;

import java.util.ArrayList;
import java.util.List;

public class AST implements Tree {
    private AST parent;
    private List<AST> children;
    private String label;

    public AST(String label){
        this.label = label;
        children = new ArrayList<>();
    }

    @Override
    public Tree getParent() {
        return parent;
    }

    @Override
    public Object getPayload() {
        return label;
    }

    @Override
    public Tree getChild(int i) {
        return children.get(i);
    }

    @Override
    public int getChildCount() {
        return children.size();
    }

    @Override
    public String toStringTree() {
        return null;
    }

    public void addChild(AST child) {
        if (child != null){
            child.parent = this;
            children.add(child);
        }
    }
}
