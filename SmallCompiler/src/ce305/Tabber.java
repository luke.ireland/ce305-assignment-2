package ce305;

import java.util.function.UnaryOperator;

public class Tabber implements UnaryOperator<String> {

    @Override
    public String apply(String s) {
        return "\t" + s;
    }
}
