package ce305;

import static org.antlr.v4.runtime.CharStreams.fromFileName;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.gui.TreeViewer;

import javax.swing.JFrame;

import java.awt.Dimension;
import java.util.List;

public class Main
{

    private final static Path i1 = Paths.get("input/in1");
    private final static Path i2 = Paths.get("input/in2");
    private final static Path i3 = Paths.get("input/in3");


    public static void showTree(AST tree)
    {
        TreeViewer ast = new TreeViewer(null, tree);
        JFrame frame = new JFrame();
        frame.add(ast);
        frame.setSize(new Dimension(500, 400));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public static void main(String[] args) throws IOException
    {
        List<Path> inputs = Arrays.asList(i1,i2,i3);
        for (Path input : inputs){
            CharStream cs = fromFileName(input.toString()); // read the input file
            CompilerLexer lexer = new CompilerLexer(cs); // create a lexer
            CommonTokenStream tokens = new CommonTokenStream(lexer); // scan stream for tokens
            CompilerParser parser = new CompilerParser(tokens); // Create a parser using tokens
            ParseTree tree = parser.start(); // Create AST parse tree from parser
            AST ast = new TreeVisitor().visit(tree);
            showTree(ast);
            PprintVisitor visitor = new PprintVisitor();
            String pprint = visitor.visit(tree); // Build string by traversing AST
            Path output = Paths.get("output/"+input.getFileName().toString() + ".py");
             try{
                FileWriter myWriter = new FileWriter(output.toString());
                myWriter.write(pprint);
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
    }
}
