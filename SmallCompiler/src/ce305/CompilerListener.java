// Generated from C:/Users/Luke/Documents/Uni/CE305/ce305-assignment-2/SmallCompiler/src\Compiler.g4 by ANTLR 4.8
package ce305;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CompilerParser}.
 */
public interface CompilerListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CompilerParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(CompilerParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link CompilerParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(CompilerParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(CompilerParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(CompilerParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code opAssign}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterOpAssign(CompilerParser.OpAssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opAssign}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitOpAssign(CompilerParser.OpAssignContext ctx);
	/**
	 * Enter a parse tree produced by the {@code newline}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterNewline(CompilerParser.NewlineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code newline}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitNewline(CompilerParser.NewlineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code defClassStmt}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterDefClassStmt(CompilerParser.DefClassStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code defClassStmt}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitDefClassStmt(CompilerParser.DefClassStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(CompilerParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(CompilerParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(CompilerParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(CompilerParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code callFunc}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterCallFunc(CompilerParser.CallFuncContext ctx);
	/**
	 * Exit a parse tree produced by the {@code callFunc}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitCallFunc(CompilerParser.CallFuncContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compStmt}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterCompStmt(CompilerParser.CompStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compStmt}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitCompStmt(CompilerParser.CompStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code return}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterReturn(CompilerParser.ReturnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code return}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitReturn(CompilerParser.ReturnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assign}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterAssign(CompilerParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assign}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitAssign(CompilerParser.AssignContext ctx);
	/**
	 * Enter a parse tree produced by the {@code defFuncStmt}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterDefFuncStmt(CompilerParser.DefFuncStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code defFuncStmt}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitDefFuncStmt(CompilerParser.DefFuncStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link CompilerParser#forStmt}.
	 * @param ctx the parse tree
	 */
	void enterForStmt(CompilerParser.ForStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link CompilerParser#forStmt}.
	 * @param ctx the parse tree
	 */
	void exitForStmt(CompilerParser.ForStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link CompilerParser#whileStmt}.
	 * @param ctx the parse tree
	 */
	void enterWhileStmt(CompilerParser.WhileStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link CompilerParser#whileStmt}.
	 * @param ctx the parse tree
	 */
	void exitWhileStmt(CompilerParser.WhileStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link CompilerParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void enterIfStmt(CompilerParser.IfStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link CompilerParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void exitIfStmt(CompilerParser.IfStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link CompilerParser#elifStmt}.
	 * @param ctx the parse tree
	 */
	void enterElifStmt(CompilerParser.ElifStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link CompilerParser#elifStmt}.
	 * @param ctx the parse tree
	 */
	void exitElifStmt(CompilerParser.ElifStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link CompilerParser#elseStmt}.
	 * @param ctx the parse tree
	 */
	void enterElseStmt(CompilerParser.ElseStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link CompilerParser#elseStmt}.
	 * @param ctx the parse tree
	 */
	void exitElseStmt(CompilerParser.ElseStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code defineClass}
	 * labeled alternative in {@link CompilerParser#defClass}.
	 * @param ctx the parse tree
	 */
	void enterDefineClass(CompilerParser.DefineClassContext ctx);
	/**
	 * Exit a parse tree produced by the {@code defineClass}
	 * labeled alternative in {@link CompilerParser#defClass}.
	 * @param ctx the parse tree
	 */
	void exitDefineClass(CompilerParser.DefineClassContext ctx);
	/**
	 * Enter a parse tree produced by the {@code defineFunc}
	 * labeled alternative in {@link CompilerParser#defFunc}.
	 * @param ctx the parse tree
	 */
	void enterDefineFunc(CompilerParser.DefineFuncContext ctx);
	/**
	 * Exit a parse tree produced by the {@code defineFunc}
	 * labeled alternative in {@link CompilerParser#defFunc}.
	 * @param ctx the parse tree
	 */
	void exitDefineFunc(CompilerParser.DefineFuncContext ctx);
	/**
	 * Enter a parse tree produced by the {@code function}
	 * labeled alternative in {@link CompilerParser#func}.
	 * @param ctx the parse tree
	 */
	void enterFunction(CompilerParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code function}
	 * labeled alternative in {@link CompilerParser#func}.
	 * @param ctx the parse tree
	 */
	void exitFunction(CompilerParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exponential}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExponential(CompilerParser.ExponentialContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exponential}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExponential(CompilerParser.ExponentialContext ctx);
	/**
	 * Enter a parse tree produced by the {@code number}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNumber(CompilerParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by the {@code number}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNumber(CompilerParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterFuncExpr(CompilerParser.FuncExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitFuncExpr(CompilerParser.FuncExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code string}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterString(CompilerParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code string}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitString(CompilerParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by the {@code floorDiv}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterFloorDiv(CompilerParser.FloorDivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code floorDiv}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitFloorDiv(CompilerParser.FloorDivContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variable}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterVariable(CompilerParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variable}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitVariable(CompilerParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplicative}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicative(CompilerParser.MultiplicativeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplicative}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicative(CompilerParser.MultiplicativeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code modulus}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterModulus(CompilerParser.ModulusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code modulus}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitModulus(CompilerParser.ModulusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code additive}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAdditive(CompilerParser.AdditiveContext ctx);
	/**
	 * Exit a parse tree produced by the {@code additive}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAdditive(CompilerParser.AdditiveContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolOp}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void enterBoolOp(CompilerParser.BoolOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolOp}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void exitBoolOp(CompilerParser.BoolOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code true}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void enterTrue(CompilerParser.TrueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code true}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void exitTrue(CompilerParser.TrueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code false}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void enterFalse(CompilerParser.FalseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code false}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void exitFalse(CompilerParser.FalseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code negBool}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void enterNegBool(CompilerParser.NegBoolContext ctx);
	/**
	 * Exit a parse tree produced by the {@code negBool}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void exitNegBool(CompilerParser.NegBoolContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relational}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void enterRelational(CompilerParser.RelationalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relational}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 */
	void exitRelational(CompilerParser.RelationalContext ctx);
}