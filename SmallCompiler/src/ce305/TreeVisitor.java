package ce305;

import java.util.ArrayList;
import java.util.List;

public class TreeVisitor extends CompilerBaseVisitor<AST> {


    @Override
    public AST visitStart(CompilerParser.StartContext ctx) {
        AST result = new AST("");
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            AST child = this.visit(stmt);
            result.addChild(child);
        }
        return result;
    }

    @Override
    public AST visitOpAssign(CompilerParser.OpAssignContext ctx) {
        String var = ctx.VAR().getText();
        String op = ctx.op.getText();
        AST child = this.visit(ctx.expr());
        String value = String.format("%s %s", var, op);
        AST result = new AST(value);
        result.addChild(child);
        return result;
    }

    // Print variable and value of assignment
    @Override
    public AST visitAssign(CompilerParser.AssignContext ctx) {
        String var = ctx.VAR().getText();
        AST child = this.visit(ctx.expr());
        String value = String.format("%s =", var);
        AST result = new AST(value);
        result.addChild(child);
        return result;
    }

    @Override
    public AST visitCompStmt(CompilerParser.CompStmtContext ctx) {
        AST result = new AST("");
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            AST child = this.visit(stmt);
            result.addChild(child);
        }
        return result;
    }

    @Override
    public AST visitIfStmt(CompilerParser.IfStmtContext ctx) {
        AST result = new AST("if");
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            AST child = this.visit(stmt);
            result.addChild(child);
        }
        if (ctx.elifStmt() != null) {
            AST child = this.visit(ctx.elifStmt());
            result.addChild(child);
        }
        if (ctx.elseStmt() != null) {
            AST child = this.visit(ctx.elseStmt());
            result.addChild(child);
        }
        return result;
    }

    @Override
    public AST visitElifStmt(CompilerParser.ElifStmtContext ctx) {
        AST result = new AST("elif");
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            AST child = this.visit(stmt);
            result.addChild(child);
        }
        return result;
    }

    @Override
    public AST visitElseStmt(CompilerParser.ElseStmtContext ctx) {
        AST result = new AST("else");
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            AST child = this.visit(stmt);
            result.addChild(child);
        }
        return result;
    }

    @Override
    public AST visitDefineClass(CompilerParser.DefineClassContext ctx) {
        String name = ctx.VAR(0).getText();
        AST result;
        if (ctx.VAR().size() > 1) {
            result = new AST(String.format("class %s(%s)", name, ctx.VAR(1).getText()));
        } else {
            result = new AST(String.format("class %s:", name));
        }
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            AST child = this.visit(stmt);
            result.addChild(child);
        }
        return result;
    }

    @Override
    public AST visitDefineFunc(CompilerParser.DefineFuncContext ctx) {
        String name = ctx.VAR(0).getText();
        List<String> argsList = new ArrayList<>();
        for (int i = 1; i < ctx.VAR().size(); i++) {
            argsList.add(ctx.VAR(i).getText());
        }
        String args = String.join(",", argsList);
        AST result = new AST(String.format("def %s(%s):", name, args));
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            AST child = this.visit(stmt);
            result.addChild(child);
        }
        return result;
    }

    @Override
    public AST visitFunction(CompilerParser.FunctionContext ctx) {
        List<String> argsList = new ArrayList<>();
        for (int i = 0; i < ctx.expr().size(); i++) {
            argsList.add(ctx.expr(i).getText());
        }
        String args = String.join(",", argsList);
        int varCount = ctx.VAR().size();
        String funcName = "";
        if (varCount > 1) {
            funcName = ctx.VAR(0).getText() + "." + ctx.VAR(1).getText();
        } else {
            funcName = ctx.VAR(0).getText();
        }
        AST result = new AST(String.format("%s(%s)", funcName, args));
        return result;
    }

    @Override
    public AST visitReturn(CompilerParser.ReturnContext ctx) {
        AST result =  new AST("return");
        result.addChild(this.visit(ctx.expr()));
        return result;
    }

    @Override
    public AST visitWhileStmt(CompilerParser.WhileStmtContext ctx) {
        AST result = new AST("while");
        result.addChild(this.visit(ctx.boolExp()));
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            AST child = this.visit(stmt);
            result.addChild(child);
        }
        return result;
    }

    @Override
    public AST visitForStmt(CompilerParser.ForStmtContext ctx) {
        int size = ctx.VAR().size();
        List<String> argsList = new ArrayList<>();
        for (int i = 0; i < ctx.VAR().size() - 1; i++) {
            argsList.add(ctx.VAR(i).getText());
        }
        String args = String.join(",", argsList);
        AST result = new AST(String.format("for %s in %s:", args, ctx.VAR(size - 1).getText()));
        for (CompilerParser.StmtContext stmt : ctx.stmt()) {
            AST child = this.visit(stmt);
            result.addChild(child);
        }
        return result;
    }

    @Override
    public AST visitString(CompilerParser.StringContext ctx) {
        return new AST(ctx.STR().getText());
    }

    @Override
    public AST visitTrue(CompilerParser.TrueContext ctx) {
        return new AST("True");
    }

    @Override
    public AST visitFalse(CompilerParser.FalseContext ctx) {
        return new AST("False");
    }

    @Override
    public AST visitNegBool(CompilerParser.NegBoolContext ctx) {
        AST result = new AST("not");
        result.addChild(this.visit(ctx.boolExp()));
        return result;
    }

    @Override
    public AST visitBoolOp(CompilerParser.BoolOpContext ctx) {
        AST left = this.visit(ctx.left);
        AST right = this.visit(ctx.right);
        AST result = new AST(ctx.op.getText());
        result.addChild(left);
        result.addChild(right);
        return result;
    }

    @Override
    public AST visitRelational(CompilerParser.RelationalContext ctx) {
        AST left = this.visit(ctx.left);
        AST right = this.visit(ctx.right);
        AST result = new AST(ctx.op.getText());
        result.addChild(left);
        result.addChild(right);
        return result;
    }

    @Override
    public AST visitModulus(CompilerParser.ModulusContext ctx) {
        AST left = this.visit(ctx.left);
        AST right = this.visit(ctx.right);
        AST result = new AST("%");
        result.addChild(left);
        result.addChild(right);
        return result;
    }

    @Override
    public AST visitExponential(CompilerParser.ExponentialContext ctx) {
        AST left = this.visit(ctx.left);
        AST right = this.visit(ctx.right);
        AST result = new AST(ctx.op.getText());
        result.addChild(left);
        result.addChild(right);
        return result;
    }

    @Override
    public AST visitMultiplicative(CompilerParser.MultiplicativeContext ctx) {
        AST left = this.visit(ctx.left);
        AST right = this.visit(ctx.right);
        AST result = new AST(ctx.op.getText());
        result.addChild(left);
        result.addChild(right);
        return result;
    }

    @Override
    public AST visitAdditive(CompilerParser.AdditiveContext ctx) {
        AST left = this.visit(ctx.left);
        AST right = this.visit(ctx.right);
        AST result = new AST(ctx.op.getText());
        result.addChild(left);
        result.addChild(right);
        return result;
    }

    @Override
    public AST visitVariable(CompilerParser.VariableContext ctx) {
        AST result = new AST(ctx.VAR().getText());
        return result;
    }

    @Override
    public AST visitNumber(CompilerParser.NumberContext ctx) {
        AST result = new AST(ctx.NUM().getText());
        return result;
    }
}
