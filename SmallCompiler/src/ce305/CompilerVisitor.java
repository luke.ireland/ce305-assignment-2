// Generated from C:/Users/Luke/Documents/Uni/CE305/ce305-assignment-2/SmallCompiler/src\Compiler.g4 by ANTLR 4.8
package ce305;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link CompilerParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface CompilerVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link CompilerParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(CompilerParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(CompilerParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opAssign}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpAssign(CompilerParser.OpAssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code newline}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewline(CompilerParser.NewlineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code defClassStmt}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefClassStmt(CompilerParser.DefClassStmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStatement(CompilerParser.ForStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(CompilerParser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code callFunc}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallFunc(CompilerParser.CallFuncContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compStmt}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompStmt(CompilerParser.CompStmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code return}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn(CompilerParser.ReturnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assign}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign(CompilerParser.AssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code defFuncStmt}
	 * labeled alternative in {@link CompilerParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefFuncStmt(CompilerParser.DefFuncStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link CompilerParser#forStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStmt(CompilerParser.ForStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link CompilerParser#whileStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStmt(CompilerParser.WhileStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link CompilerParser#ifStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStmt(CompilerParser.IfStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link CompilerParser#elifStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElifStmt(CompilerParser.ElifStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link CompilerParser#elseStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseStmt(CompilerParser.ElseStmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code defineClass}
	 * labeled alternative in {@link CompilerParser#defClass}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefineClass(CompilerParser.DefineClassContext ctx);
	/**
	 * Visit a parse tree produced by the {@code defineFunc}
	 * labeled alternative in {@link CompilerParser#defFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefineFunc(CompilerParser.DefineFuncContext ctx);
	/**
	 * Visit a parse tree produced by the {@code function}
	 * labeled alternative in {@link CompilerParser#func}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(CompilerParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exponential}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExponential(CompilerParser.ExponentialContext ctx);
	/**
	 * Visit a parse tree produced by the {@code number}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(CompilerParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncExpr(CompilerParser.FuncExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code string}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(CompilerParser.StringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code floorDiv}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloorDiv(CompilerParser.FloorDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variable}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(CompilerParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicative}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicative(CompilerParser.MultiplicativeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code modulus}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModulus(CompilerParser.ModulusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code additive}
	 * labeled alternative in {@link CompilerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditive(CompilerParser.AdditiveContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolOp}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolOp(CompilerParser.BoolOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code true}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrue(CompilerParser.TrueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code false}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFalse(CompilerParser.FalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code negBool}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegBool(CompilerParser.NegBoolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relational}
	 * labeled alternative in {@link CompilerParser#boolExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelational(CompilerParser.RelationalContext ctx);
}