grammar Compiler;

tokens {
	INDENT,
	DEDENT
}

@lexer::header {
  import com.yuvalshavit.antlr4.DenterHelper;
}

@lexer::members {
  private final DenterHelper denter = new DenterHelper(NL,
                                                       CompilerParser.INDENT,
                                                       CompilerParser.DEDENT)
  {
    @Override
    public Token pullToken() {
      return CompilerLexer.super.nextToken();
    }
  };

  @Override
  public Token nextToken() {
    return denter.nextToken();
  }
}

NL: ('\r'? '\n' ' '*); // note the ' '*
WS: [ \t] -> skip;

start: stmt*;

stmt:
	VAR op = (INC | DEC) expr									# opAssign
	| VAR ASSIGN expr											# assign
	| stmt SEMICOLON stmt										# compStmt
	| defClass													# defClassStmt
	| defFunc													# defFuncStmt
	| ifStmt	                                                # ifStatement
	| whileStmt					                                # whileStatement
	| forStmt		                                            # forStatement
	| func														# callFunc
	| NL														# newline
	| RETURN expr                                               # return;

forStmt: FOR VAR+ IN (VAR | func) COLON INDENT stmt+ DEDENT;
whileStmt: WHILE boolExp COLON INDENT stmt+ DEDENT;
ifStmt: IF boolExp COLON INDENT stmt+ DEDENT elifStmt? elseStmt?;
elifStmt: ELIF boolExp COLON INDENT stmt+ DEDENT;
elseStmt: ELSE COLON INDENT stmt+ DEDENT;

defClass:
	CLASS VAR (BRAC VAR? KETS)? COLON INDENT stmt+ DEDENT # defineClass;
defFunc:
	DEF VAR BRAC VAR? (COM VAR)* KETS COLON INDENT stmt+ DEDENT # defineFunc;

func: (VAR '.')? VAR BRAC expr? (COM expr)* KETS # function;

expr:
	func											# funcExpr
	| left = expr op = POW right = expr             # exponential
	| left = expr op = FLR right = expr             # floorDiv
	| left = expr op = (MULT | DIV) right = expr	# multiplicative
	| left = expr op = (ADD | SUB) right = expr		# additive
	| left = expr MOD right = expr					# modulus
	| NUM											# number
	| STR											# string
	| VAR											# variable;
boolExp:
	TRUE														# true
	| FALSE														# false
	| NOT boolExp												# negBool
	| left = boolExp op = (AND | OR) right = boolExp			# boolOp
	| left = expr op = (LS | LSQ | EQU | GR | GRQ) right = expr	# relational;

CLASS: 'class';
FOR: 'for';
IN: 'in';
COM: ',';
DEF: 'def';
RETURN: 'return';
BRAC: '(';
KETS: ')';
INC: '+=';
DEC: '-=';
ASSIGN: '=';
IF: 'if';
ELIF: 'elif';
ELSE: 'else';
WHILE: 'while';
POW: '**';
FLR: '//';
MULT: '*';
DIV: '/';
ADD: '+';
SUB: '-';
MOD: '%';
COLON: ':';
SEMICOLON: ';';
TRUE: 'true';
FALSE: 'false';
NOT: 'not';
AND: 'and';
OR: 'or';
LS: '<';
LSQ: '<=';
EQU: '==';
GR: '>';
GRQ: '>=';
NUM: SUB? [0-9]+ ('.' [0-9]+)?;
STR: ('"' VAR '"' | '\'' VAR '\'');
VAR: [a-zA-Z][a-zA-Z0-9]*;